"use strict";

const min = 1;
const max = 10;

const secondsInDay = 24 * 60 * 60;

function getRandomIntegerInRange (from, to) {
    if (from >= to) {
        return "Incorrect input";
    }

    return Math.floor(Math.random() * (to - from + 1)) + from;
}

function checkNameInput() {
    let name;

    do {
        name = prompt(`Enter your name: `);
    } while (name === null || name === "")

    return name;
}

const name = checkNameInput();
const admin = name;
const days = getRandomIntegerInRange(min, max)
const daysInSeconds = days * secondsInDay;

console.log("name: " + name);
console.log("admin: " + admin);
console.log("days: " + days);
console.log("days in seconds: " + daysInSeconds);



